" Odpal pathogen (ładowanie modułów)
source ~/.vim/bundle/vim-pathogen/autoload/pathogen.vim
execute pathogen#infect()
" Wymuś uruchomienie vim-sensible - by dalej móc to nadpisać
runtime! plugin/sensible.vim

" Wyłączone w Debianie (jak zwykle na wszelki wypadek)
set modelines=30
set modeline

" Dużo kolorów w terminalu
if ! has("gui_running")
  set t_Co=256
endif

" feel free to choose :set background=light for a different style
set background=dark
colors peaksea

":set guifont=Terminus\ Bold\ 9
:set guifont=Terminus\ Bold\ 11
":set guifont=DejaVu\ Sans\ Mono\ 10
":set guifont=Terminus\ Bold\ 12
":set guifont=Bitstream\ Vera\ Sans\ Mono\ 7

:syntax on
:colors evening

" rozpoznawanie plików
:set filetype=on
filetype plugin indent on

autocmd BufEnter *.c :set cindent
autocmd BufEnter *.h :set cindent
autocmd BufEnter *.cpp :set cindent
autocmd BufEnter *.hpp :set cindent
autocmd BufEnter *.java :set cindent
autocmd BufEnter *.cpp :set fo=croql
autocmd BufEnter *.hpp :set fo=croql
autocmd BufEnter *.c :set fo=croql
autocmd BufEnter *.h :set fo=croql
autocmd BufEnter *.java :set fo=croql
autocmd BufEnter *.py :set nocindent autoindent
autocmd BufEnter * :set foldmethod=syntax nofoldenable

"au BufWinLeave * mkview
"au BufWinEnter * silent loadview

:set autoindent

":source ${HOME}/.vim/vimDebug/vimDebug.vim
":source ${HOME}/.vim/vimDebug/multvals.vim

" ustawienia do LLVM
  augroup filetype
    au! BufRead,BufNewFile *.ll     set filetype=llvm
    au! BufRead,BufNewFile *.llx    set filetype=llvm
  augroup END

  augroup filetype
    au! BufRead,BufNewFile *.td     set filetype=tablegen
  augroup END

" ustawienia typowe
set ai
set ts=4 sts=4 et sw=4
autocmd FileType html set ai et sw=2 ts=2 sts=2
autocmd FileType css set ai et sw=2 ts=2 sts=2
autocmd FileType javascript set ai et sw=4 ts=4 sts=4

:set laststatus=2 " zawsze pokazuj statusline

" Dziwne czcionki do Powerline
let g:Powerline_symbols = 'fancy'
":set guifont=Bitstream\ Vera\ Sans\ Mono\ for\ Powerline\ 10

" Pokaż niewidoczne (http://vimcasts.org/episodes/show-invisibles/)
nmap <leader>l :set list!<CR>
set listchars=tab:▸\ ,eol:¬

